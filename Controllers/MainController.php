<?php

$servername = "localhost";
$username = "nurmaan_unilec";
$password = "T_6NVPm}Z7kN";

$GLOBALS['conn'] = mysqli_connect($servername,$username,$password,'nurmaan_unilecture');
$GLOBALS['cookie'] = "";
$GLOBALS['count'] = 0;
$GLOBALS['modules'] = [];

setCooky();

function checkCourse(){
	$primary = mysqli_query($GLOBALS['conn'], "SELECT * FROM `module` WHERE 1 ORDER BY id DESC");
	while ($ret = mysqli_fetch_assoc($primary)){
        $GLOBALS['modules'] [] = $ret;
	}
	fetch();
}

function setCooky(){

	//Getting cookies for the first time and save them
    println("Setting cookies...");
    $username = "nurmaan";
    $password = "tree4cat";

    $dir = $_SERVER["DOCUMENT_ROOT"]."/temp";

    $path = tempnam($dir,"123");

    $url="http://lcms.uom.ac.mu/lms/login/index.php"; 
    $postinfo = "username=".$username."&password=".$password;

    $cookie_file_path = $path."/cookie.txt";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
    curl_setopt($ch, CURLOPT_USERAGENT,
        "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
    $response = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);

    $hd =  explode("Set-Cookie:", $header);
    $body = substr($response, $header_size);

    
    for ($i=1; $i < sizeof($hd); $i++) { 
        $new = explode(";", $hd[$i]);
        if($new[0] != ' MOODLEID_=deleted'){
            str_replace(' ', '', $new[0]);
            $GLOBALS['cookie'] .= $new[0].';';
        }
        
    }

    checkCourse();
    curl_close($ch);
}

function fetch(){
    //Accessing the link to check lectures
    if($GLOBALS['count'] < sizeof($GLOBALS['modules'])){
    	println('Accessing module: '.$GLOBALS['modules'][$GLOBALS['count']]['name']);
    	$id = $GLOBALS['modules'][$GLOBALS['count']]['id'];
    	$link = "http://lcms.uom.ac.mu/lms/course/view.php?id=".$id;
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $link);
	    curl_setopt($ch, CURLOPT_COOKIE, $GLOBALS['cookie']);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_REFERER,  $_SERVER['REQUEST_URI']);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, 'id='.$id);
	    $html = curl_exec($ch);
	    getLecture($html, $id);
	    curl_close($ch); 
    }    
  
}

function getLecture($html, $identity){
	println('Checking lecture notes...');
	$needle = '<a   href="http://lcms.uom.ac.mu/lms/mod/resource/view.php?id=';
	$lastPos = 0;
	$noteID = [];
	$noteName = [];
	$note = [];

	while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
		$idStart = strpos($html, "id=", $lastPos) + 3;
		$idEnd = strpos($html, "\"", $idStart);
		$idLength = $idEnd - $idStart;
	    $id = substr($html, $idStart, $idLength);
        
        $nameStart = strpos($html, "<span>", $idEnd) + 6;
        $nameEnd = strpos($html, "<span", $nameStart);
        $nameLength = $nameEnd - $nameStart;
	    $name = substr($html, $nameStart, $nameLength);
	    $note[] = ['id'=> $id, 'name' => $name];
	    $lastPos = $lastPos + strlen($needle);
	}
    

    $available = getTotalLecture($identity);
    println("Number of lectures: ".$available);

    $required = sizeof($note);
    println("Number of new lectures found: ". sizeof($note));
    if($required > $available){
    	$offset = $required - $available;
    	for ($i = $required - 1; $i >= $available; $i--) { 
    		downloadLecture($note[$i]);
    	}
    	println($offset.' new note(s) saved for this module</br>====================================================</br>');
    }else{
    	println('No new notes to save from this module</br>====================================================</br>');
    }
    $GLOBALS['count']++;
    fetch();

}

function downloadLecture($note){
	$id = $note['id'];
	$name = $note['name'];
	println ("Downloading lecture ".$id." ~~~~~~ Temporary name: ".$name);
    $host = "http://lcms.uom.ac.mu/lms/mod/resource/view.php?id=".$id;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, false);
    curl_setopt($ch, CURLOPT_COOKIE, $GLOBALS['cookie']);
    curl_setopt($ch, CURLOPT_REFERER,  $_SERVER['REQUEST_URI']);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'id='.$id);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $fp;

    if(strpos($result, '<html') !== false && strpos($result, 'errormessage') === false){
    	println('Indirect download. Getting link url...');
    	$start = strpos($result, "http://lcms.uom.ac.mu/lms/file.php");
	    $end = strpos($result, "\"", $start);
	    $length = $end - $start;
	    $download = substr($result, $start, $length);
	    println("Accessing link: ".$download);
	    
	    $fnStart = strrpos($download, "/") + 1;
	    $filename = substr($download, $fnStart);
	    $output_filename = "notes/".$filename;
	    println("Filename: ".$filename);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $download);
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_AUTOREFERER, false);
	    curl_setopt($ch, CURLOPT_COOKIE, $GLOBALS['cookie']);
	    curl_setopt($ch, CURLOPT_REFERER,  $_SERVER['REQUEST_URI']);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    $file = curl_exec($ch);
	    curl_close($ch);

	    $fp = fopen($output_filename, 'w');
	    if(fwrite($fp, $file) !== false){
            saveNote($download, $name, $filename,1);            
	    }
	    fclose($fp);

    }elseif(strpos($result, '<html') === false && strpos($result, 'errormessage') === false){
    	println('Direct download. Downloading file...');
    	$output_filename = "notes/".$name;
    	$fp = fopen($output_filename, 'w');
	    if(fwrite($fp, $result) !== false){
            saveNote($host, $name, "",1);
	    }
	    fclose($fp);
    }else{
        saveNote($host, $name, "", 0);
    }
    
}

function saveNote($link, $originalName, $name, $valid){
    println('Saving note...');
	if(mysqli_query($GLOBALS['conn'], "INSERT INTO `lectureNotes` (origin, originalName, name, module_id, valid) VALUES ('".$link."', '".$originalName."', '".$name."', ".$GLOBALS['modules'][$GLOBALS['count']]['id'].", ".$valid.")")){
		println('Note '.$name.' saved');
		println("");
	}

}

function getTotalLecture($id){
	return mysqli_num_rows(mysqli_query($GLOBALS['conn'], "SELECT * FROM `lectureNotes` WHERE module_id = ".$id));
}


function println($msg){
    echo($msg."</br>");
}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}